import axios from 'axios';
import qs from 'qs';

export const LOGOUT="LOGOUT";
export const LOGIN="LOGIN";
export const REGISTER="REGISTER"
const url='http://localhost:8080'

export function LogIn(user,callback){
    const loginPromise =axios.post(
        `${url}/login`,
        qs.stringify(user),
        {withCredentials:true}
        )
        .then(res1 => {
            console.log("res1",res1);
            callback(res1);
            return res1.data;
        })
        .catch(err => {
            return {
                success: false
            };
        });
        return {
            type: LOGIN,
            payload:loginPromise
        }

}

export function logout(callback){
    const logoutPromise = axios.post(`${url}/logout`,{withCredentials:true})
        .then(res => {
            callback(res);
            return res;
        })
        .catch(err => {
            return {
                success: false
            };
        });
    return {
        type: LOGOUT,
        payload: logoutPromise
    }
}

export function registeruser(user,callbcak){
    const registerPromise = axios.post(`${url}/users`,user,{withCredentials:true})
        .then(res => {
            console.log("register res",res);
            callbcak(res);
            return res;
        });
    return {
        type:REGISTER,
        payload:registerPromise
    }
}