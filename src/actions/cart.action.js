import axios from "axios/index";

export const ADD_TO_CART='ADD_TO_CART';
export const REMOVE_ITEM='REMOVE_ITEM';
export const DECREMENT_QUANTITY='DECREMENT_QUANTITY';
export const INCREMENT_QUANTITY='INCREMENT_QUANTITY';
const url='http://localhost:8080'
export function addToCart(id){
    const addToCartPromise = axios.get(`${url}/products/${id}`)
        .then(res=>{
                    return {
                        success:true,
                        addProduct:res.data // product
                    }
        })
        .catch(err=>{
            return{
                success:false
            }
        });
  return{
      type:ADD_TO_CART,
      payload:addToCartPromise
  }
}

export function removeItem(id){

    return{
        type:REMOVE_ITEM,
        payload:id
    }
}

export function decrement(id){
    return{
        type: DECREMENT_QUANTITY,
        payload:id
    }
}

export function increment(id){
    return{
        type: INCREMENT_QUANTITY,
        payload:id
    }
}