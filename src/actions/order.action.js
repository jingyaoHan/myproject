import axios from 'axios';

export const ADD_ORDER='ADD_ORDER';
const url='http://localhost:8080'
export function addOrder(order){
    const addOrderPromise=axios.post(`${url}/orders`,order)
        .then( res => {
             return{
                 order:order,
                 success:res.data.success,
             }
            })
        .catch(err=>{
            return{
                success:false
            }
        });
       return {
           type:ADD_ORDER,
           payload:addOrderPromise
       }

}