import axios from 'axios'
export const GET_PRODUCTS='GET_PRODUCTS';
export const EDIT_PRODUCT='EDIT_PRODUCT';
export const ADD_PRODUCT='ADD_PRODUCT';
export const DELETE_PRODUCT='DELETE_PRODUCT';
const url='http://localhost:8080'
export function  getProducts(){
    const getProductsPromise = axios.get(`${url}/products`);
    return {
        type: GET_PRODUCTS,
        payload:getProductsPromise
    }
}

export function addProduct(product) {
    const addProductPromise = axios.post(`${url}/products`,product)
        .then(res=>{
            return {
                success:res.data.success,
                newProduct:product
            };
        })
        .catch(err=>{
            return{
                success:false
            }
        });
    return{
        type:ADD_PRODUCT,
        payload:addProductPromise
    }
}

export function deleteProduct(id){
    const deleteProductPromise = axios.delete(`${url}/products/${id}`)
        .then(res=>{
            return{
                success:res.data.success,
                id:id
            }
        })
        .catch(err=>{
            return{
                success:false
            }
        });
    return{
        type:DELETE_PRODUCT,
        payload:deleteProductPromise
    }
}

export function editProduct(){
    const editProductPromise = axios.put(`${url}/products`);
      return {
          type:EDIT_PRODUCT,
          payload:editProductPromise
      }
}
