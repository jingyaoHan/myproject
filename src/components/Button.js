import styled from 'styled-components'

export  const ButtonContainer= styled.button`
        text-transform: capitalize;
        font-size: 16px;
        background: transparent;
        border: 3px solid;
        border-color: #009FFD;
        border-color:${props => props.cart ? "#FFD700" : "#009FFD"}
        color: ${props=>props.cart?"#FFD700" : "#009ffd"};
        border-radius:3px;
        padding: 0.2 rem 0.5 rem;
        cursor: pointer;
        margin:0.2rem 0.5 rem 0.2rem 0.2;
        transition:all 0.5s ease-in-out;
        &: hover{
        background: ${props=>props.cart?"#FFD700" : "#009ffd"};
        color: purple;
        }
        &:focus{
        outline:none;
        }
    `