import React,{Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {Button} from "bootstrap";
import Title from "./Title";
import CartColumns from "./CartColumns";
import EmptyCart from "./EmptyCart";
import CartList from "./CartList";
import CartTotals from "./CartTotals";
import {decrement, increment, removeItem} from "../actions/cart.action";
import {addOrder} from "../actions/order.action";



 class Cart extends Component{


    constructor(props){
        super(props);
        this.state={
            cartSubtotal:0,
            cartTaxt:0,
            cartTotal:0,
            order:{
                status:'pending',
                total:0
            }
        }
    }
    checkOutHandler=()=>{
         let order=this.props.cart;
         order.purchase_date=new Date();
         const userId=JSON.parse(localStorage.getItem('user')).id
         order.user={id:userId};

         console.log(order);
        this.props.addOrder(order);
    }

     removeItem=(id)=>{
         this.props.removeItem(id);
         this.forceUpdate();
     }
     increment=(id)=>{
         this.props.increment(id);
         this.forceUpdate();
     }
     decrement=(id)=>{
         this.props.decrement(id);
         this.forceUpdate();
     }
    checkCart=()=>{
         if (this.props.cart.purchases.length>0){
             return(
                 <>
                     <Title name="your" title="cart"></Title>
                     <CartColumns />
                     <CartList items={this.props.cart.purchases}
                               removeItem={this.removeItem}
                               increment={this.increment}
                               decrement={this.decrement}
                     />
                     <CartTotals subTotal={this.props.cart.total}/>
                     <div className="text-center">
                         <Link to="/payment">
                         <button className="btn btn-outline-primary text-uppercase mb-3 px-5" onClick={this.checkOutHandler}>
                             CheckOut
                         </button>
                         </Link>
                     </div>

                 </>
             )
         }else{
             return(
                 <EmptyCart/>
             )
         }
     }

    render(){
        return (
            <section>
                {
                   this.checkCart()
                }
            </section>
        )


    }
}

function mapStateToProps({cart}) {
    return{cart}
}
export default connect(mapStateToProps,{removeItem,increment,decrement,addOrder})(Cart);