import React from 'react'
import {connect} from "react-redux";



 export default class CartItem extends React.Component{
    constructor(props){
        super(props);
        this.state={

            total:0
        }
    }




    render(){
        return(
            <div className="row my-2 text-capitalize text-center">
                <div className="col-10  col-lg-2">
                    <img src={this.props.item.product.image} style={{width:'5rem' , height:"5rem"}}
                    className="img-fluid" alt="product"
                    />
                </div>
                <div className="col-10  col-lg-2">
                    <span className="d-lg-none">product:</span>
                    {this.props.item.product.name}
                </div>
                <div className="col-10  col-lg-2">
                    <span className="d-lg-none">price:</span>
                    {this.props.item.product.price}
                </div>
                    <div className="col-10 col-lg-2 my-2 my-lg-0">
                        <div className="d-flex justify-content-center">
                            <div>
                                <button className="btn  mx-1" onClick={()=>{this.props.decrement(this.props.item.product.id)}}>
                                    -
                                </button>
                                <span className="btn mx-1">
                                    {this.props.item.qty}
                                </span>
                                <button className="btn  mx-1" onClick={()=>{this.props.increment(this.props.item.product.id)}}>
                                    +
                                </button>
                            </div>
                        </div>
                    </div>
                <div className="col-10 col-lg-2">
                    <button onClick={()=>{this.props.removeItem(this.props.item.product.id)}}>
                        <i className="fa fa-trash"> </i>
                    </button>
                </div>
                <div className="col-10 col-lg-2">
                    <strong>item total : $   {this.props.item.product.price * this.props.item.qty.toFixed(2)}</strong>
                </div>

            </div>
        )
    }

}

