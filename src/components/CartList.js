import React ,{Component} from 'react'
import CartItem from "./CartItem";


export default class CartList extends Component{
    constructor(props){
        super(props);
    }

    render(){
        console.log(this.props.items);
        return(
            <div className="container-fluid">
                {
                    this.props.items && this.props.items.map(item=>{
                        return(
                            <CartItem key={item.id} item={item}
                                      removeItem={this.props.removeItem}
                                      increment={this.props.increment}
                                      decrement={this.props.decrement}
                            />
                        )
                    })
                }

            </div>
        )
    }

}
