import React,{Component} from 'react';
import {Link} from "react-router-dom";

export default class CartTotals extends Component{
    constructor(props){
        super(props);
        this.state={
            cartSubtotal:0,
            cartTax:0,
            cartTotal:0
        }
    }

    render(){
        return(
            <>
                <div className="container">
                    <div className="row">
                        <div className="col-10 mt-2 ml-sm-5 ml-md-auto col-sm-8 text-capitalize text-right">
                            <h5>
                                <span className="text-title">
                                    subtotal:
                                </span>
                            <strong>${this.props.subTotal}</strong>
                        </h5>
                            <h5>
                                <span className="text-title">
                                    tax:
                                </span>
                                <strong>${(this.props.subTotal * 0.05).toFixed(2)}</strong>
                            </h5>
                            <h5>
                                <span className="text-title">
                                    total:
                                </span>
                                <strong>${(this.props.subTotal*1.07).toFixed(2)}</strong>
                            </h5>
                        </div>
                    </div>
                </div>
            </>
            )

    }
}