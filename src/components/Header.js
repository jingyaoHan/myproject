import React from 'react';
import Link from "react-router-dom/es/Link";
import logo from "../logo.svg";
import {ButtonContainer} from "./Button";
import styled from 'styled-components';

export const Header = () => {

   const style = {
        width: '150px',
        height: '100px',
    };




    return(
        <NavWrapper className="navbar navbar-expand-sm navbar-light px-sm-5">

                <Link to="/">
                    <img src={logo} alt="store"  style={style} className="navbar-brand" title="Back to home page"></img>
                </Link>
                <ul className="navbar-nav align-items-center">
                    <li className="nav-item ml-5">
                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    <li> <Link className="nav-link" to="/login">Login</Link></li>
                    <li> <Link className="nav-link" to="/logout">Logout</Link></li>
                    <li>  <Link className="nav-link" to="/register">Register</Link></li>
                    <li>  <Link className="nav-link" to="/users">Users</Link></li>
                    <li>  <Link className="nav-link" to="/admin-list">Admin-list</Link></li>
                </ul>


            <Link to="/cart" className="ml-auto">
                <ButtonContainer>
                    <i className="fa fa-opencart"></i>
                    my cart
                </ButtonContainer>
            </Link>
        </NavWrapper>
    )
}

const NavWrapper = styled.nav`
    // background: #FFDEAD;
     background: #AFEEEE
    .nav-link{
    color: #ADD8E6;
    font-size:1.3rem;
    text-transform: capitalize
    }
`

