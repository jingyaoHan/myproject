import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {ButtonContainer} from "./Button";
import {connect} from "react-redux";
import {addToCart} from "../actions/cart.action";


class ProductDetails  extends Component{

    constructor(props){
        super(props);

    }

    handleClick=(id)=>{
        console.log(id);
        this.props.addToCart(id);
    }
    render(){
        return(
          <div className="container py-5">

              <div className="row">
                    <div className="col-10 mx-auto col-md-6   my-3 " >
                        <img src={this.props.product.image} className="img-fluid" alt="product" height="450px" width="450px"/>
                    </div>

                  <div className="col-10 mx-auto col-md-6
                    my-3 text-capitalize">
                    <h2>{this.props.product.name}</h2>
                      <h4 className="text-title text-uppercase text-muted mt-3 mb-2">
                          made by: <span className="text-uppercase">
                          {this.props.product.brand}
                      </span>
                      </h4>
                      <p className="text-capitalize font-weight-bold mt-3 mb-0">
                          some info about the product:
                      </p>
                      <p className="text-muted lead">
                          {this.props.product.detail}
                      </p>
                      <h4 className="text-danger">
                          <strong>
                             Price: <span>$</span>
                              {this.props.product.price}
                          </strong>
                      </h4>
                      {/*buttons*/}
                      <div>
                          <Link to="/">
                              <ButtonContainer>
                                  Back to home
                              </ButtonContainer>
                          </Link>&nbsp;&nbsp;&nbsp;
                          <ButtonContainer onClick={() => {this.handleClick(this.props.product.id)}} cart>
                              Add to Cart
                          </ButtonContainer>
                      </div>
                  </div>
              </div>
          </div>
        )
    }
}

function mapStateToProps({products},componentProps){
    const id=+componentProps.match.params.id;
    const product=products &&  products.find(p=>p.id===id);
    return {
        products,
        product
    };
}

export default connect(mapStateToProps,{addToCart})(ProductDetails);