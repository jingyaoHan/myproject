import React from 'react';
import Title from './Title'
import {connect} from "react-redux";
import {getProducts} from "../actions/products.action";
import Product from "../containers/Product";



class ProductList extends React.Component{

    style = {
        display:'flex box'
    };
   constructor(props){
       super(props);
       props.getProducts();
   }

        render(){
        return(
            <>

                <div className="py-5">
                    <div className="container" style={this.style}>
                      <Title name="Your" title="Best Choice"/>
                            <div>
                                {
                                    this.props.products && this.props.products.map((p,index) => {
                                       return(

                                           <Product product={p} key={index} />

                                           // {/*<div  className="card" key={index}>*/}
                                           //     {/*<div className="card-image">*/}
                                           //         {/*<img*/}
                                           //             {/*src={p.image}*/}
                                           //             {/*alt={p.name}*/}
                                           //             {/*style={this.style}/>*/}
                                           //         {/*<span className="card-title">{p.name}</span>*/}
                                           //         {/*<span to="/" className="btn-floating halfway-fab waves-effect waves-light red" onClick={()=>{this.handleClick(p.id)}}>*/}
                                           //             {/*<i className="fa fa-plus-circle">Add to cart</i>*/}
                                           //         {/*</span>*/}
                                           //     {/*</div>*/}
                                           //     {/*<div className="card-content">*/}
                                           //         {/*<p><b>Price: {p.price}$</b></p>*/}
                                           //     {/*</div>*/}
                                           // {/*</div>*/}


                                       )
                                    })
                                }
                            </div>
                    </div>

                </div>
            </>

        )
    }
}
function mapStateToProps({products}){
    return {products};
}
export default connect(mapStateToProps,{getProducts})(ProductList);