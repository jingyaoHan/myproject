import React, {Component} from 'react'
import {connect} from "react-redux";

export default function (ExistingComponent) {
    class WrapperHOCComponent extends Component{
        constructor(props){
            super(props);
            this.state={};
        }

        static getDerivedStateFromProps(props,currentState){
            if(props.login !== 'logged in'){
                props.history.push('/login');
                console.log(props.login);
            }
            console.log("currentState",currentState);
            return currentState;
        }
        render(){
            return(
                <ExistingComponent {...this.props}/>
            );
        }
    }
    function mapStateToProps({login}) {
        return {login};
    }
    return connect(mapStateToProps)(WrapperHOCComponent)
}