import React from 'react';
import Button from "@material-ui/core/es/Button/Button";
import {connect} from "react-redux";
import {addProduct} from "../actions/products.action";
import {Link} from "react-router-dom";

const PRODUCT_FIELDS=['name','brand','type','price','stock','detail','image'];
class AddProduct extends React.Component{
    constructor(props){
        super(props);
        this.state={
            newProduct:{},
        }
    }
    updateControl(event){
        const newProduct={...this.state.newProduct};
        newProduct[event.target.id]=event.target.value;
        console.log(event.target.value);
        this.setState({newProduct});
    }
    renderControl(field){
        const type=field === 'price' || field === 'stock' ? 'number' : 'text';
        return(
            <div key={field} className="form-group">
                <label htmlFor={field} className="text-capitalize">{field}</label>
                <input
                    type={type}
                    id={field}
                    className="form-control"
                    name={field}
                    onInput={this.updateControl.bind(this)}
                />
            </div>
        )
    }
    submit =(event) =>{
        event.preventDefault();
        console.log(this.state.newProduct);
        this.props.addProduct(this.state.newProduct);
    }

    render(){
        return(
            <main className="container">
                <h2>Add Product</h2>
                <form onSubmit={this.submit}>
                    {
                        PRODUCT_FIELDS.map(field=>this.renderControl(field))
                    }

                    <Button variant="contained" color="primary" type="submit">
                        Add Product
                    </Button>

                </form>
            </main>
        )
    }
}
export default connect(null,{addProduct})(AddProduct);
