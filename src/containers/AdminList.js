import React from 'react';
import {connect} from "react-redux";
import {deleteProduct, getProducts} from "../actions/products.action";
import {ButtonContainer} from "../components/Button";
import {Link} from "react-router-dom";
import Button from "react-bootstrap/es/Button";

class  AdminList  extends React.Component{
    // style = {
    //     width: '100px',
    //     height: '80px'
    // };
    constructor(props){
        super(props);
        this.state=[];
        this.props.getProducts();

    }

    removeProductHandler=(id)=>{
      console.log(id);
        this.props.deleteProduct(id);
        const index=this.props.products.findIndex(product=>{
            return id === product.id;
        });
        this.props.products.splice(index,1);
        this.setState(this.props.products);
        // this.forceUpdate();
    }

    render(){
        return (
            <div>
            <table className="table table-bordered table-striped">
                <thead className="text-center text-capitalize text">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Brand</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Detail</th>
                        <th>Image</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {
                    this.props.products && this.props.products.map((p,index)=>{
                        return(
                             <tr key={index}>
                                 <td>{p.id}</td>
                                 <td>{p.name}</td>
                                 <td>{p.brand}</td>
                                 <td>{p.type}</td>
                                 <td>{p.price}</td>
                                 <td>{p.stock}</td>
                                 <td style={{wordBreak:'break-all'}}>{p.detail}</td>
                                 <td><img src={p.image} style={{width:'80px',height:'80px'}} /></td>
                                 <td>
                                     <Link to={`/edit-product/${p.id}`}>
                                     <ButtonContainer><i className="fa fa-edit"></i></ButtonContainer> &nbsp;
                                     </Link>


                                     <ButtonContainer onClick={()=>this.removeProductHandler(p.id)}><i className="fa fa-trash"></i></ButtonContainer>
                                 </td>
                             </tr>
                        )
                    })
                }
                </tbody>
            </table>

                <div className="float-lg-right">
                    <Link to="/add-product">
                    <Button variant="primary" size="lg">
                        Add Product
                    </Button>
                    </Link>
                </div>
            </div>


        )
    }
}

function mapStateToProps({products}) {
    return {products};
}

export default connect(mapStateToProps,{getProducts,deleteProduct})(AdminList);