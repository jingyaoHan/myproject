import React from 'react';
import {Field, reduxForm} from "redux-form";
import {connect} from "react-redux";
import {editProduct, getProducts} from "../actions/products.action";
import Button from "react-bootstrap/es/Button";
import {MDBBtn} from "mdbreact";


class EditProduct extends  React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        !this.props.products && this.props.getProducts();
    };

    renderField(field) {
        const type = field.input.name === 'price' || field.input.name === 'stock' ? 'number' : 'text'
        // console.log("field", field);
        return (
            <div className="form-group">
                <label htmlFor={field.input.name} className="text-capitalize text-primary">{field.input.name}</label>
                <input
                    className="from-control"
                    type={type}
                    id={field.input.name}
                    {...field.input}
                />
                <div style={{color: 'red'}}>{field.meta.error}</div>
            </div>
        )
    }
    submit=(event)=>{
        console.log(event.target.input.value);
        // this.props.editProduct(event);
    }

    render() {
        return (
            <main className="container">
                <h2>Edit Product {this.props.match.params.id}</h2>
                <form onSubmit={this.submit}>
                    <Field
                        name="name"
                        component={this.renderField}
                    />
                    <Field
                        name="brand"
                        component={this.renderField}
                    />
                    <Field
                        name="type"
                        component={this.renderField}
                    />
                    <Field
                        name="price"
                        component={this.renderField}
                    />
                    <Field
                        name="stock"
                        component={this.renderField}
                    />
                    <Field
                        name="detail"
                        component={this.renderField}
                    />
                    <Field
                        name="image"
                        component={this.renderField}
                    />
                    <Button variant="success" type="submit" >Submit</Button>
                </form>


            </main>
        );
    };
}
    function mapStateToProps({products,form},componentProps){
        const id=+componentProps.match.params.id;// //match from react router, get the id from url
        const product=products && products.find(p=>p.id===id);
        return {
            products,
            initialValues:product
        };
    }
    export default connect(mapStateToProps,{getProducts,editProduct})(reduxForm({
        form:'EditProductForm',
        enableReinitialize:true,
        validate:null
    })(EditProduct));
