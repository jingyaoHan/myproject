import React from 'react';
import {Field, reduxForm} from "redux-form";
import Button from "@material-ui/core/es/Button/Button";
import {connect} from "react-redux";
import {LogIn} from "../actions/auth.actions";



class Login extends React.Component{
    constructor(props){
        super(props);
        this.state={
           message:''
        };
    }
    renderField(field){
        const type=field.input.name === 'password' ? 'password':'text'
        // console.log(field);
        return(
            <div className="form-group" key={field}>
                <label htmlFor={field.input.name}>{field.input.name}</label>
                <input
                    type={type}
                    className="form-control"
                    id={field.input.name}
                    {...field.input}
                />
            </div>
        )
    }
    submit = (user) => {
        this.props.LogIn(user,(res)=>{
            if(res.data.success){
                localStorage.setItem('user',JSON.stringify(res.data.user));
                this.setState({
                    message:'Login success!'
                });
            }else{
                this.setState({
                    message:'Username or Password is not correct!'
                });
            }
        });
        // this.props.history.push('/');
    }
    render(){
        return (
            <main className="container">
                <h2>Login</h2>
                <form onSubmit={this.props.handleSubmit(this.submit)}>
                    <Field
                        name="username"
                        component={this.renderField}
                    />

                    <Field
                        name="password"
                        type="password"
                        component={this.renderField}
                    />


                    <Button vatiant="contained" color="primary" type="submit">
                        <i className="fa fa-sign-in-alt"></i>
                        Login
                    </Button>



                </form>
            </main>
        )
    }
}

export default connect(null,{LogIn})(reduxForm({
    form:'LoginForm',
    validate:null
})(Login));