import React, {Component} from 'react';
import {connect} from "react-redux";
import {logout} from "../actions/auth.actions";
import Button from 'react-bootstrap/Button';
import Modal from "react-bootstrap/es/Modal";


class Logout extends Component{
    constructor(props){
        super(props);
        this.handleShow=this.handleShow.bind(this);
        this.handleClose=this.handleClose.bind(this);
        this.state={
            show:false,
        };
    }
    handleShow(){
        this.setState({show:true});
    }
    handleClose(){
        this.setState({show:false});
    }
    handleLogout = () => {
        this.setState({show:false});
        this.props.logout((res)=>{


            if(res.data && res.data.success){
                localStorage.removeItem('user');
                this.props.history.push('/home');
            }
        });
    }
    render(){
        return(
            <>
                <Button variant="primary" onClick={this.handleShow}>
                    Logout
                </Button>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Hi {JSON.parse(localStorage.getItem('user')).username}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure to log out?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleClose}>
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={this.handleLogout} >
                            Logout
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
            // {/*<div id="logout">*/}
            //     {/*<label className="label label-danger">Are you sure to log out?</label><br/>*/}
            //     {/*<button onClick={this.handleLogout}>Logout</button>*/}
            // {/*</div>*/}
        )
    }
}
export default connect (null,{logout})(Logout);