import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {addToCart} from "../actions/cart.action";


 class Product extends React.Component{

    constructor(props){
        super(props);

    }
     handleAddToCartClick=(id)=>{
         console.log(id);
         this.props.addToCart(id);
     }

    render(){
        return(

            <ProductWrapper className="col-9 mx-auto col-lg-3 my-4">
               <div className="card">
                          <div className="img-container p-7"  key={this.props.index} onClick={this.handleClick}>
                               <Link to={`/product-details/${this.props.product.id}`}>
                                   <img src={this.props.product.image} alt="product" className="card-img-top"></img>
                               </Link>
                              <button className="cart-btn" onClick={() => {this.handleAddToCartClick(this.props.product.id) }}>
                                  <i className="fa fa-cart-plus"/>
                              </button>
                           </div>
                   {/*card footer*/}
                   <div className="card-footer d-flex justify-content-between">
                       <p className="align-self-center mb-0">
                           {this.props.product.name}
                       </p>
                       <h5 className="font-italic mb-0">
                           <span className="mr-1">$</span>
                           {this.props.product.price}
                       </h5>
                   </div>
               </div>

            </ProductWrapper>
        )
    }
}
function mapStateToProps({products}){
    return {products};
}
export default connect(mapStateToProps,{addToCart})(Product);


const ProductWrapper = styled.div`
   .card{
      border-color:transparent;
      transition:all 0.3s linear; 
   }
   .card-footer{
   background:transparent;
   border-top:transparent;
    transition:all 0.3s linear;
   }
   &:hover{
        .card{
        border:0.04rem solid rgba(0,0,0,0.2);
        box-shadow:17px 17px 20px 0px rgba(0,0,0,0.2)
        }
         .card-footer{
            background:rgba(247,247,247)
            
         }
   }
   
   .img-container{
    position:relative;
    overflow:hidden;
   }
   .card-img-top{
           transition:all 1s linear;
   }
   .img-container:hover .card-img-top{
   transform:scale(1.3);
   }
   .cart-btn{
     position:absolute;
     bottom:0;
     right:0;
     padding:0.2rem 0.4rem;
     border:none;
     color: rgb(255,255,255);
     font-size:1.4rem;
     border-radius:0.5rem 0 0 0;
     transform:translate(100%,100%);
            transition:all 1s linear;
     background: #87CEFA
   
   }
   .img-container:hover .cart-btn{
    transform:translate(0,0);
   }
   .cart-btn:hover{
   color:#0000FF;
   cursor:pointer;
   }
`
