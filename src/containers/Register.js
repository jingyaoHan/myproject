import React, {Component} from 'react';
import {Field, reduxForm} from "redux-form";
import Button from "@material-ui/core/es/Button/Button";
import {connect} from "react-redux";
import {registeruser} from "../actions/auth.actions";


class Register extends Component{
    constructor(props){
        super(props);
        this.state ={
            message:''
        };
    }
    onSubmit=(user)=>{
        const{username, password, cfpass}=user;
        this.props.registeruser({username,password},(res)=>{
            console.log("register res",res);
            if(res.data.success){
                this.setState({
                    message:'Register Success!'
                });
            }else{
                this.setState({
                    message:'Register Fail!'
                });
            }
        });
    }

    renderField(field){
        return(
            <div className="form-group" key={field}>
                <label htmlFor={field.input.name} >{field.input.name.charAt(0).toUpperCase()+field.input.name.slice(1)}</label>
                    <input
                        type={field.type}
                        className="form-control"
                        id={field.input.name}
                        {...field.input}
                    />

            </div>
        )
    }
    render(){
        return(
            <div id="register">
                <h2>Register a New User</h2>
                <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Field
                    name="username"
                    lable="Username"
                    type="text"
                    component={this.renderField}
                    />
                    <Field
                    name="password"
                    label="Password"
                    type="password"
                    component={this.renderField}
                    />
                    <Field
                    name="Confirm Password"
                    lable="Confirm Password"
                    type="password"
                    component={this.renderField}
                    />
                    <Button variant="contained" color="primary" type="submit">
                        Submit
                    </Button>
                    <label className="text-danger">{this.state.message}</label>
                </form>
            </div>
        )
    }
}

export default connect(null,{registeruser})(reduxForm({
    form:'RegisterForm',
    validate:null
})(Register));