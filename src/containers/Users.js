import React from "react";
import {connect} from "react-redux";
import {getUsers} from "../actions/user.action";


class Users extends React.Component{
    constructor(props){
        super(props);
        props.getUsers();
        console.log(props.users);
    }
    render(){
       return(
           <table className="table table-bordered table-striped">
               <thead>
                    <tr>
                        <th>user ID</th>
                        <th>username</th>
                    </tr>
               </thead>
               <tbody>
               {
                   this.props.users && this.props.users.map((u,index) =>{
                       return(
                           <tr key={index}>
                               <td>{u.id}</td>
                               <td>{u.username}</td>
                           </tr>
                       )
                   })
               }
               </tbody>
           </table>
       );
    }
}
function mapStateToProps({users}){
    return {users};
}

export default connect(mapStateToProps,{getUsers})(Users);