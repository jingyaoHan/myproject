import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/APP';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import {rootReducer} from "./reducers/root.reducer";
import ReduxPromise from 'redux-promise';
import {BrowserRouter, Route} from "react-router-dom"
import Login from "./containers/Login";
import { Switch } from "react-router-dom"
import Users from "./containers/Users";
import auth from "./components/auth.hoc";
import Logout from "./containers/Logout";
import Register from "./containers/Register";
import ProductList from "./components/ProductList";
import ProductDetails from "./components/ProductDetails";
import Default from "./components/Default";
import Cart from "./components/Cart";
import AdminList from "./containers/AdminList";
import EditProduct from "./containers/EditProduct";
import AddProduct from "./containers/AddProduct";
import Payment from "./containers/Payment";
import OrderList from "./containers/OrderList";
import Checkout from "./containers/Checkout";

const createStoreWithMiddleware=applyMiddleware(ReduxPromise)(createStore);
ReactDOM.render(
    <Provider store={createStoreWithMiddleware(rootReducer)}>
        <BrowserRouter>
            <App>
                <Switch>
                    <Route path="/users" component={auth(Users)}/>
                    {/*<Route path="/users" component={Users}/>*/}
                    <Route path="/login" component={Login}/>
                    <Route path="/logout" component={Logout}/>
                    <Route path="/order-list" component={OrderList}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/checkout" component={Checkout}/>
                    <Route path="/payment" component={Payment}/>
                    <Route path="/add-product" component={AddProduct}/>
                    <Route path="/edit-product/:id" component={EditProduct}/>
                    <Route path="/product-details/:id" component={ProductDetails}/>
                    <Route path="/cart" component={Cart}/>
                    <Route path="/admin-list" component={AdminList} />
                    <Route path="/" component={ProductList}/>
                    <Route component={Default}/>
                </Switch>
            </App>
        </BrowserRouter>
    </Provider>

    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
 