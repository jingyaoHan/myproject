import {LOGIN, LOGOUT, REGISTER} from "../actions/auth.actions";

export function authReducer(state=false,action){
    let res;
    switch(action.type){
        case LOGIN:
           res=action.payload;
           if(res.success){
               return res.user;
           }else{
               return false;
           }

        case LOGOUT:
            res = action.payload.data
            if(res.success){
                return false;
            }else{
                return true;
            }

        case REGISTER:
            return action.payload;
        default:
            return state;
    }
}

