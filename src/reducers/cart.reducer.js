import {ADD_TO_CART, DECREMENT_QUANTITY, INCREMENT_QUANTITY, REMOVE_ITEM} from "../actions/cart.action";

const iniState={
    total:0,
    status: 'pending',
    purchases: []
}

export function cartReducer(state=iniState,action){
    switch(action.type){
        case ADD_TO_CART:
            // console.log("addToCart payload",[...state,action.payload.addProduct])
            const index= state.purchases.findIndex((orderProduct)=>{
                return action.payload.addProduct.id === orderProduct.product.id
            })
            if(state.purchases.length>0 && index >-1){
                let newPurchases=[...state.purchases]
                newPurchases[index].qty +=1;
                return Object.assign(state, {purchases: newPurchases, total: state.total+action.payload.addProduct.price});
            }else{
                let newPurchases=[...state.purchases,
                    { qty:1,
                        product:action.payload.addProduct
                    }]
                return Object.assign(state,{purchases:newPurchases, total: state.total+action.payload.addProduct.price});
            }

        case REMOVE_ITEM:
           let newPurchases=[...state.purchases]
            const removeIndex=state.purchases.findIndex((orderProduct)=>{
                return action.payload === orderProduct.product.id
            })
            const removeTotal= newPurchases[removeIndex].qty*newPurchases[removeIndex].product.price;
            newPurchases.splice(removeIndex,1);
           return Object.assign(state,{purchases:newPurchases, total:state.total-removeTotal});

        case INCREMENT_QUANTITY:
            const incrementIndex= state.purchases.findIndex((orderProduct)=>{
                return action.payload === orderProduct.product.id
            })
            let newPurchases1=[...state.purchases];
            newPurchases1[incrementIndex].qty +=1;
            return Object.assign(state, {purchases: newPurchases1, total: state.total+newPurchases1[incrementIndex].product.price});


        case DECREMENT_QUANTITY:
                  const decrementIndex=  state.purchases.findIndex((orderProduct)=>{
                      return action.payload === orderProduct.product.id })
            if(state.purchases[decrementIndex].qty<=1){
                return state;
            }else{
                let newPurchases2=[...state.purchases];
                newPurchases2[decrementIndex].qty -=1;
                return Object.assign(state, {purchases: newPurchases2, total: state.total-newPurchases2[decrementIndex].product.price});
            }


        default:
            return state;
    }

}