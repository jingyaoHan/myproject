import {ADD_ORDER} from "../actions/order.action";


export function orderReducer(state=[],action){
    switch (action.type) {
        case ADD_ORDER:
            if(action.payload.success){
                return [...state,action.payload.order];
            }else{
                return state;
            }
        default:
            return state;

    }
}