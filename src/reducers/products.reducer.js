import {ADD_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT, GET_PRODUCTS} from "../actions/products.action";

export function productsReducer(state=null,action){
    switch (action.type){
        case GET_PRODUCTS:
            console.log(action.payload.data);
            return action.payload.data;

        case ADD_PRODUCT:
            if(action.payload.success){
                return [...state,action.payload.newProduct];
            }
            return state;

        case DELETE_PRODUCT:

            const index= state.findIndex(product=>{
                return action.payload.id === product.id;
            })
            console.log("state",state);
            let newProducts=[...state]
            newProducts.splice(index,1);
            return Object.assign(state,{products:newProducts});



        case EDIT_PRODUCT:
            return action.payload.data;
        default:
            return state;
    }
}