import {authReducer} from "./auth.reducer";
import {combineReducers} from "redux";
import {reducer as formReducer } from 'redux-form';
import {usersReducer} from "./users.reducer";
import {productsReducer} from "./products.reducer";
import {cartReducer} from "./cart.reducer";
import {orderReducer} from "./order.reducer";

export const rootReducer = combineReducers({
    login:authReducer,
    users:usersReducer,
    products:productsReducer,
    cart:cartReducer,
    orders:orderReducer,
    form:formReducer

})