import {GET_USERS} from "../actions/user.action";

export function usersReducer(state=null,action) {
    switch (action.type){
        case GET_USERS:
            console.log("action.payload",action.payload);
            return action.payload.data;
        default:
            return state;
    }
}